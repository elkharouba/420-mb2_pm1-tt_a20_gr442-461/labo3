package com.example.labo3;

import androidx.appcompat.app.AppCompatActivity;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

public class GPSLocationActivity extends AppCompatActivity {
    Button btnGps ;

    EditText ed_country;
    EditText ed_province;
    EditText ed_ville;
    GPSLocationTracket gps;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Geocoder geocoder = new Geocoder(this);
        this.btnGps = (Button) findViewById(R.id.btnGps);

        this.ed_country = (EditText) findViewById(R.id.ed_country);
        this.ed_province = (EditText) findViewById(R.id.ed_province);
        this.ed_ville = (EditText) findViewById(R.id.ed_ville);




        this.btnGps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gps = new GPSLocationTracket(GPSLocationActivity.this);
                List<Address> addressList = null;
                if(gps.canGetLocation())
                {
                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();

                    try
                    {
                      addressList = geocoder.getFromLocation(latitude,longitude,1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    String pays = addressList.get(0).getCountryName();
                    String province = addressList.get(0).getAdminArea();
                    String ville = addressList.get(0).getLocality();


                    Toast.makeText(getApplicationContext(),"Votre position est \nLat:"
                            + latitude + "\nLongitude : " + longitude
                             , Toast.LENGTH_LONG).show();

                    //ed_lati.setText(latitude));
                    //ed_logi.setText(longitude);
                    ed_ville.setText(ville.toString());
                    ed_country.setText(pays.toString());
                    ed_province.setText(province.toString());
                }else
                    {
                        gps.showSettingsAlert();
                    }
            }
        });
    }
}
















